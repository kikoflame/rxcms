Chào bạn, nếu đây là lần đầu tiên bạn sử dụng hệ thống, hãy dành một vài phút đọc nội dung này để biết cách sử dụng.
#### Bạn có thể quản lý

  - Người Dùng
  - Trang Web
  - Tập Tin và Thư Mục
  - Phần mềm mở rộng

#### Làm thế nào để quản lý người dùng ?
1. Bấm vào mục trình đơn ```Hệ Thống``` và chọn ```Quản Lý Người Dùng```
2. Xem danh sách các tác vụ ở bên trái, trong đó bao gồm
 * Mời một người dùng vào không gian làm việc của bạn bằng cách sử dụng email
 * Tạo và xoá người dùng
 * Thay đổi và phân công vai trò khác nhau cho người dùng

#### Làm thế nào để quản lý các trang web ?
1. Bấm vào mục trình đơn ```Hệ Thống``` và chọn ```Quản Lý Trang```
2. Ở đó, bạn có thể quản lý thông tin trang web, trong đó bao gồm
 * Tên trang web
 * Mô tả trang web

#### Làm thế nào để quản lý các tập tin và thư mục ?
1. Bấm vào mục trình đơn ```Quản Lý Tệp```
2. Chọn một thư mục có một trong những tên sau ```download```, ```js```, ```css``` và ```imgs```
3. Mỗi thư mục nên chứa một số loại tập tin nhất định
 * ```download``` là nơi được sử dụng để lưu trữ các tập tin mà bạn muốn người dùng cuối tải về. Định dạng tập tin cho phép là: PDF, ZIP, RAR, tập tin hình ảnh ( GIF, PNG, JPG, JPEG )
 * ```js``` là nơi mà bạn có thể lưu trữ các tập tin javascript của trang web. Vì vậy , bạn nên tải lên các tập tin javascript.
 * ```css``` là nơi mà bạn có thể lưu trữ các tập tin css của trang web. Vì vậy , bạn nên tải lên các tập tin css.
 * ```imgs``` là nơi mà bạn có thể lưu trữ các tập tin hình ảnh của trang. Vì vậy , bạn nên tải lên các tập tin hình ảnh( GIF, PNG, JPG, JPEG ).

#### Làm thế nào để quản lý phần mềm mở rộng?
1. Bấm vào mục trình đơn ```Quản Lý Phần Mềm Mở Rộng```
2. Mỗi phần mềm mở rộng có thuộc tính của riêng nó. Bạn nên tham khảo hướng dẫn sử dụng để biết thêm thông tin