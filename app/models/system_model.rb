require 'bundler'

class SystemModel

  def self.get_list_of_compatible_gems
    all = Bundler.load.specs.find_all do |g|
      if (defined?(g.metadata))
        g.metadata['compatCode'] == ENV['compatibility_code']
      end
    end

    all
  end

  protected

end