# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
MIN_COVERAGE = 75
THRESHOLD_COVERAGE = 80
FAIL_BUILD_ON_LOW_COVERAGE = true
REPORTS_PATH = File.expand_path('../../reports', __FILE__)
INTEGRATION_TESTING_REPORT_PATH = File.expand_path('../../reports/integration', __FILE__)

require 'simplecov'
SimpleCov.start 'rails' do
  SimpleCov.coverage_dir('reports/coverage')
  at_exit do
    SimpleCov.result.format!
    if FAIL_BUILD_ON_LOW_COVERAGE and SimpleCov.result.covered_percent < MIN_COVERAGE
      $stderr.puts "Build has failed! Coverage is less than #{MIN_COVERAGE}%. Test coverage must be kept above #{THRESHOLD_COVERAGE}%."
      exit 1
    end

    if SimpleCov.result.covered_percent < THRESHOLD_COVERAGE
      $stderr.puts "Warning! Coverage is less than #{THRESHOLD_COVERAGE}%. Please try to keep test coverage above #{THRESHOLD_COVERAGE}%."
      exit 0
    end
  end
end

require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'rspec/autorun'
require 'capybara/rspec'
require 'capybara/rails'
require 'capybara/poltergeist'

require 'authlogic/test_case'
include Authlogic::TestCase

# Integration testing generator
require 'support/integration_test_generator'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }

# Initialize instance of IntegrationTestGenerator
generator = Support::IntegrationTestGenerator.new(INTEGRATION_TESTING_REPORT_PATH)

RSpec.configure do |config|
  # Capybara Config
  config.include Capybara::DSL
  Capybara.javascript_driver = :poltergeist

  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  # If true, the base class of anonymous controllers will be inferred
  # automatically. This will be the default behavior in future versions of
  # rspec-rails.
  config.infer_base_class_for_anonymous_controllers = false

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = "random"

  # Factory Girl verbosity
  config.include FactoryGirl::Syntax::Methods

  # Exclude Integration Tests By Default
  config.filter_run_excluding integration: true

  # TODO: Once executed regardless of tags (either unit or integration test), all files under "integration" folder will be deleted (no workarounds have been found)

  if !File.directory?(REPORTS_PATH)
  	Dir.mkdir(REPORTS_PATH)
  end

  if !File.directory?(INTEGRATION_TESTING_REPORT_PATH)
    Dir.mkdir(INTEGRATION_TESTING_REPORT_PATH)
  else
  	FileUtils.rm_rf(Dir.glob("#{INTEGRATION_TESTING_REPORT_PATH}/*"))
  end

  FileUtils.copy_file(File.expand_path('../../app/assets/javascripts/manager/jquery-1.9.1.min.js', __FILE__), "#{INTEGRATION_TESTING_REPORT_PATH}/jquery-1.9.1.min.js")

  generator.generate_header

  config.after :each, :integration => true do |t|
    if !t.example.exception.nil?
      page.save_screenshot("#{INTEGRATION_TESTING_REPORT_PATH}/#{t.example.metadata[:full_description].gsub(/ /,'_')}.png")
      generator.generate_elements(false, t)
    else
      generator.generate_elements(true, t)
    end
  end

end
