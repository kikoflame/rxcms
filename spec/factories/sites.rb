# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :site do
    factory :site_admin do
      id 1
      sites_id 'AF323'
      available true
    end

    factory :site_dev do
      id 9999999
      sites_id 'AIS33'
      available true
    end
  end
end
