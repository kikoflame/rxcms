Welcome user, if this is the first time you have used the CMS, please spend a few minutes reading this for a quick start.
#### You can manage

  - Users
  - Sites
  - Files & Folders
  - Plugins

#### How do I manage users?
1. Click on menu item ```System``` and choose ```Users & Roles```
2. Make your choice by browsing a list of operations on the left, which include
 * Inviting a user to your workspace by using email
 * Creating and deleting users
 * Changing and assigning different roles to users

#### How do I manage sites?
1. Click on menu item ```System``` and choose ```Sites Preferences```
2. There, you can manage site information, which includes
 * Site name
 * Site description

#### How do I manage files and folders?
1. Click on menu item "Files"
2. Pick a category which is amongst ```download```, ```js```, ```css``` and ```imgs```
3. Each category should have its unique types of file
 * ```download``` is the place which is used for storing files that you want end-user to download. Allowed file formats are: PDF, ZIP, RAR, Images Files (GIF, PNG, JPG, JPEG)
 * ```js``` is the place in which you can store your site's javascript files. So, you should upload javascript files there.
 * ```css``` is the place in which you can store your site's stylesheet files. So, you should upload cascading stylesheet files there.
 * ```imgs``` is the place in which you can store your site's images files. So, you should upload image files (GIF, PNG, JPG, JPEG) there.

#### How do I manage plugins?
1. Click on menu item ```extensions```
2. Each extension should have their own configuration. Please refer to their configuration manual for more information