#!/bin/sh

cd ${PWD%/*}

echo 'preparing database for INTEGRATION testing...'
rake db:setup RAILS_ENV=integration

echo 'performing INTEGRATION testing...'
RAILS_ENV=integration rspec --tag integration --order default

