class setup_rvm {
  include rvm
  rvm::system_user { vagrant: ; }

  $my_ruby_version = 'ruby-2.0.0-p247'

    rvm_system_ruby {
      $my_ruby_version:
	    ensure => 'present',
        default_use => false;
    }

    rvm_gem {
      'bundler':
        name => 'bundler',
        ruby_version => $my_ruby_version,
        ensure => latest,
        require => Rvm_system_ruby[$my_ruby_version];
    }
}