class setup_postgresql {
  package {
    ["postgresql", "postgresql-server-dev-9.1"]:
      ensure => present
  }
}