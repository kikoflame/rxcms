module Manager::ManagerHelper

  # Check site access
  def site_access
    if (session[:accessible_app].nil?)
      raise t(:msg_no_site_selected)
    end
    if (session[:accessible_roleid].nil?)
      raise t(:msg_no_role_selected)
    end
  end

  # Get a list of applications of users
  def get_users_applications

    if (UserSession.find.nil?)
      raise t(:msg_user_not_login_yet)
    end

    # user = User.find(UserSession.find.record.id)
    userSites = SiteUser.find_all_by_users_id(UserSession.find.record.id).map { |t| t.sites_id }.uniq

    tArray = Array.new
    userSites.each do |t|
      tSiteMetadata = Metadata.first({ :conditions => ['sites_id = ? and cat = ?', t.to_s, 'site'] })

      if (!tSiteMetadata.nil?)
        tSiteMetadataPrefs = ActiveSupport::JSON.decode(tSiteMetadata.value)
        tHash = Hash.new
        tHash['id'] = tSiteMetadata.sites_id.to_s
        tHash['name'] = tSiteMetadataPrefs['name'].strip

        tArray << tHash
      end
    end
    return tArray
  end

end