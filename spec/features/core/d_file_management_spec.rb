require 'spec_helper'

feature "File Management" do
  before do
    sign_in build(:validUser)
  end

  scenario "Administrator should see a list of folders", :integration => true, :js => true do
    visit("/manager/cms/files")
    page.should have_content("download"), "download folder is not found"
  end

  scenario "User can attach image file to upload", :integration => true, :js => true do
    visit("/manager/cms/files")
    page.should have_content("download"), "download folder is not found"
    within("#uploadDiv") do
      fill_in 'filePath', :with => '/downloads'
      fill_in 'fileCategory', :with => 'png'
      attach_file 'fileUpload_datafile', Rails.root.join('spec','fixtures','files','rspec.png')
      find("input[name=commit]", :visible => false).trigger('click')
    end
    all('input[type=checkbox]', :visible => false)[1].trigger('click')
    # page.should have_content('rspec.png'), 'file not uploaded properly'
    # page.save_screenshot('upload_test_image.png')
  end

  scenario "User can attach css file to upload", :integration => true, :js => true do
    visit("/manager/cms/files")
    page.should have_content("css"), "css folder is not found"
    within("#uploadDiv") do
      fill_in 'filePath', :with => '/css'
      fill_in 'fileCategory', :with => 'css'
      attach_file 'fileUpload_datafile', Rails.root.join('spec','fixtures','files','text.css')
      find("input[name=commit]", :visible => false).trigger('click')
    end
    page.should have_content("css"), "css folder is not found"
    page.should have_content('uploaded'), 'file not uploaded properly'
    all('input[type=checkbox]', :visible => false)[0].trigger('click')
    page.should have_content('text.css'), 'file is not listed'

    # also view it
    click_on 'text.css'

    # also delete it
    all('.dataDelete', :visible => false)[0].trigger('click')
    page.should have_selector('a', :text => 'Proceed')
    find('a', :text => 'Proceed').trigger('click')
    page.should have_content('deleted'), 'file not deleted properly'
  end

  scenario "User can attach js file to upload", :integration => true, :js => true do
    visit("/manager/cms/files")
    page.should have_content("js"), "js folder is not found"
    within("#uploadDiv") do
      fill_in 'filePath', :with => '/js'
      fill_in 'fileCategory', :with => 'js'
      attach_file 'fileUpload_datafile', Rails.root.join('spec','fixtures','files','text.js')
      find("input[name=commit]", :visible => false).trigger('click')
    end
    page.should have_content("js"), "js folder is not found"
    page.should have_content('uploaded'), 'file not uploaded properly'
  end

end