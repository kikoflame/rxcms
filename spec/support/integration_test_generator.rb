module Support
  class IntegrationTestGenerator
    def initialize(path)
      @integration_testing_report_path = path
    end

    def generate_header
      file = File.open("#{@integration_testing_report_path}/index.html", "w+")
      file.write("<style>h1,h3 {color:black;font-family:Arial,sans-serif} ul {list-style-type:none;margin:8px 0 8px 0;padding:0;} li {font-family:Arial,sans-serif}</style>")
      file.write("<script type=\"text/javascript\" src=\"jquery-1.9.1.min.js\"></script>")
      file.write("<script>$(function(){ $('#test-count').text($('ul').length.toString()); $('#test-failed-count').text($('.failed').length.toString()); $('#test-passed-count').text($('.passed').length.toString()); });</script>")
      file.write("<h1>Integration Test Summary&nbsp;(<span id=\"test-count\"></span>&nbsp;examples)</h1>")
      file.write("<h3><span style=\"color:red;\">Failed:</span>&nbsp;<span id=\"test-failed-count\">0</span></h3>")
      file.write("<h3><span style=\"color:green;\">Passed:</span>&nbsp;<span id=\"test-passed-count\">0</span></h3>")
      file.close
    end

    def generate_elements(result_code, obj)
      if (result_code)
        itemsHtml = "<ul class=\"passed\"><li><span style=\"color:green;\">&#10004;</span>&nbsp;<span>#{DateTime.now.strftime('%m/%d/%Y %H:%M:%S')}</span>&nbsp;<a href=\"#\"><strong>[#{obj.example.metadata[:full_description].gsub(obj.example.description,'').strip}]</strong> #{obj.example.description}</a></li></ul>"
      else
        itemsHtml = "<ul class=\"failed\"><li><span style=\"color:red;\">&#10008;</span>&nbsp;<span>#{DateTime.now.strftime('%m/%d/%Y %H:%M:%S')}</span>&nbsp;<a href=\"#{obj.example.metadata[:full_description].gsub(/ /,'_')}.png\"><strong>[#{obj.example.metadata[:full_description].gsub(obj.example.description,'').strip}]</strong> #{obj.example.description}</a><br /><p style=\"padding-left:16px;font-size:14px;color:red;\"><strong>Details:&nbsp;</strong><span>#{obj.example.exception.to_s}</span></p></li></ul>"
      end

      file = File.open("#{@integration_testing_report_path}/index.html", "a")
      file.write(itemsHtml)
      file.close
    end
  end
end