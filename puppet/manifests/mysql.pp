class setup_mysql {
  package {
    ["mysql-client", "mysql-server", "libmysqlclient-dev"]:
      ensure => present
  }
}