# Be sure to restart your server when you modify this file.

# Your secret key for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!
# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.

if Rails.env.production? && ENV['RXCMS_SECRET_TOKEN'].blank?
  raise "The environment variable RXCMS_SECRET_TOKEN must be set in production environments!"
end

Rxcms::Application.config.secret_token = ENV['RXCMS_SECRET_TOKEN'] ||
  '29b3f735dd05bcbbf33e6f12ce172030342a882c8b4279bdb3158822f3a40a305a3e1cf810b40b2560048f7122741645b7803cadff78a7042b30a0a52d387a7b'