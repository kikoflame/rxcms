class Manager::ManagerController < ApplicationController
  include ApplicationHelper, Manager::ManagerHelper

  layout 'manager'

  before_filter :get_current_user_role

  before_filter :get_current_build, :except => [
      :login,
      :register,
      :select_application
  ]
  before_filter :get_current_user_ui_menuitem_visibility, :except => [
      :login,
      :register,
      :select_application
  ]
  before_filter :valid_owner?, :except => [
      :login,
      :register,
      :select_application
  ]
  before_filter :site_takendown?, :except => [
      :login,
      :register,
      :select_application
  ]

  before_filter :site_access, :except => [
      :login,
      :register,
      :select_application
  ]

  before_filter :isEnabledOfs?, :except => [
      :login,
      :register,
      :select_application
  ]

  # Show home page
  def home
    if (@curUserRole == 'anonymous' ||
        @curUserRole == 'loggedin')
      raise t(:msg_unauthorized_access)
    end

    cookies[:url] = request.original_url

    @mPage = 'index'

    @mountedExts = SystemModel.get_list_of_compatible_gems

    # Load users
    @allUsersOfSite = Array.new
    tAllUsersOfSite = SiteUser.all({ :conditions => ['sites_id = ?', session[:accessible_appid]], :order => 'created_at DESC' }).take(5).uniq_by { |u| u.users_id }
    tAllUsersOfSite.each do |t|
      @allUsersOfSite << User.find(t.users_id)
    end

    # Load getting started
    ## Initialize Markdown parser
    if File.exists?(Rails.root.join("config","locales","docs","getting_started.#{@curUserRole}.#{I18n.locale}.md"))
      markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML, :autolink => true, :space_after_headers => true)

      tGettingStartedFile = File.new(Rails.root.join("config","locales","docs","getting_started.#{@curUserRole}.#{I18n.locale}.md"))
      tGettingStartedContent = ''
      while (line = tGettingStartedFile.gets)
        tGettingStartedContent << line
      end
      tGettingStartedFile.close
      @gettingStarted = markdown.render(tGettingStartedContent)
    else
      @gettingStarted = t(:getting_started_file_missing)
    end

    respond_to do |t|
      t.html
    end
  end

  # Show login page
  def login
    if (@curUserRole == 'user')
      raise 'unauthorized access'
    elsif (@curUserRole == 'admin' ||
        @curUserRole == 'developer' ||
        @curUserRole == 'contentadmin' ||
        @curUserRole == 'loggedin')
      if (session[:accessible_app].nil?)
        # redirect_to '/manager/cms/apps'
        if (cookies[:last_app].nil? || cookies[:last_app].empty?)
          firstSite = SiteUser.first({ :conditions => ['users_id = ?', session[:accessible_userid]] })
          if (!firstSite.nil?)
            session[:accessible_app] = Site.find(firstSite.sites_id).nil? ? nil : Site.find(firstSite.sites_id).sites_id
            session[:accessible_appid] = firstSite.sites_id
            session[:accessible_roleid] = firstSite.roles_id

            cookies[:last_app] = firstSite.sites_id
            cookies[:last_role] = firstSite.roles_id

            # Get site name
            get_site_name(firstSite.sites_id)

            redirect_to '/manager/cms'
          else
            redirect_to '/manager/cms/apps'
          end
        else
          if (!cookies[:last_role].nil? && !cookies[:last_role].empty?)
            if (!SiteUser.first({ :conditions => ['sites_id = ? and users_id = ? and roles_id = ?', cookies[:last_app], session[:accessible_userid], cookies[:last_role] ] }).nil?)
              session[:accessible_app] = Site.find(cookies[:last_app]).nil? ? nil: Site.find(cookies[:last_app]).sites_id
              session[:accessible_appid] = cookies[:last_app]
              session[:accessible_roleid] = cookies[:last_role]

              get_site_name(cookies[:last_app])

              redirect_to '/manager/cms'
            else
              session[:accessible_app] = nil
              session[:accessible_appid] = nil
              session[:accessible_roleid] = nil

              cookies.delete :last_app
              cookies.delete :last_role

              redirect_to '/manager/cms/apps'
            end
          end
        end
      else
        redirect_to '/manager/cms/home'
      end
      return
    end

    respond_to do |t|
      t.html { render :layout => 'authentication' }
    end
  end

  # Show register page for invitation
  def register
    if (@curUserRole != 'anonymous')
      raise t(:msg_unauthorized_access)
    end

    # Check if user is existing for the current invitation, if it is, assign system user to workspace
    invitation = Invitation.first({ :conditions => ['invitation_id = ? and accepted = ?', params[:invitation_id], false] })

    if (invitation.nil?)
      raise t(:msg_invitation_not_exist)
    else
      invitedUser = User.find_by_email(invitation.email)
      invitationsByEmail = Invitation.find_all_by_email(invitation.email)
      foundInvitationByEmail = false
      foundInvitationAcceptedAlready = false

      invitationsByEmail.each do |t|
        if (t.accepted == true and t.invitation_id == params[:invitation_id] )
          foundInvitationByEmail = true
          break
        end
      end

      if (foundInvitationByEmail)
        raise t(:msg_invitation_claimed)
      end

      if (invitedUser.nil?)
        respond_to do |t|
          t.html { render :layout => 'authentication' }
        end
      else
        siteUser = SiteUser.first({ :conditions => ['users_id = ? and sites_id = ?', invitedUser.id, invitation.sites_id] })
        invitationsByEmail.each do |t|
          if (t.accepted == true and t.sites_id == invitation.sites_id and !siteUser.nil?)
            foundInvitationAcceptedAlready = true
            break
          end
        end

        if (foundInvitationAcceptedAlready)
          raise t(:msg_invitation_already_accepted_in_other)
        end

        SiteUser.create({
                            :sites_id => invitation.sites_id,
                            :users_id => invitedUser.id,
                            :is_owner => false,
                            :roles_id => invitation.roles_id
                        })
        Invitation.update(invitation.id, { :accepted => true })

        if (!UserSession.find.nil?)
          UserSession.find.destroy
        end
        reset_session

        flash[:warn] = t(:user_accepted_invitation_successfully)
        redirect_to '/manager/cms'
      end

    end
  end

  # Select application on login
  def select_application
    if (@curUserRole == 'user' ||
        @curUserRole == 'anonymous')
      raise t(:msg_unauthorized_access)
    end

    @userSites = get_users_applications

    respond_to do |t|
      t.html { render :layout => 'authentication'}
    end
  end

  # Show view called "pages" and its view variables
  def pages

    if (@curUserRole == 'admin' ||
        @curUserRole == 'contentadmin' ||
        @curUserRole == 'user' ||
        @curUserRole == 'anonymous' ||
        @curUserRole == 'loggedin')
      raise t(:msg_unauthorized_access)
    end

    cookies[:url] = request.original_url

    @mPage = 'home'

    @listOfTemplates = Metadata.all({:conditions => ['cat = ? and sites_id = ?', 'template', session[:accessible_appid]]})
    @pages = Metadata.all({:conditions => ['cat = ? and sites_id = ? and flags = ?', 'page', session[:accessible_appid], 0]})

    @mountedExts = SystemModel.get_list_of_compatible_gems
    respond_to do |t|
      t.html
    end
  end

  # Show view called "elements" and its view variables
  def elements

    if (@curUserRole == 'admin' ||
        @curUserRole == 'contentadmin' ||
        @curUserRole == 'user' ||
        @curUserRole == 'anonymous' ||
        @curUserRole == 'loggedin')
      raise t(:msg_unauthorized_access)
    end

    cookies[:url] = request.original_url

    @mPage = 'elements'

    metadata = Metadata.all({ :conditions => ['sites_id = ? and flags = ?', session[:accessible_appid], 0]})
    tPagesArray = Array.new

    metadata.each do |t|
      tHash = Hash.new
      tPageItem = t.cat.to_s.strip

      if (tPageItem == 'template')
        tHash['id'] = t.id.to_s.strip
        tHash['key'] = t.key.to_s.strip
        tPagesArray << tHash
      end
    end

    @pages = tPagesArray

    tLayoutsArray = Array.new

    metadata.each do |t|
      tHash = Hash.new
      tLayoutItem = t.cat.to_s.strip

      if (tLayoutItem == 'placeholder')
        tHash['id'] = t.id.to_s.strip
        tHash['key'] = t.key.to_s.strip
        tLayoutsArray << tHash
      end
    end

    @layouts = tLayoutsArray

    # Get a list of files for styles
    @css = Metadata.all({:conditions => ['cat = ? and sites_id = ? and flags = ?', 'css', session[:accessible_appid], 0]})
    # Get all javascripts
    @js = Metadata.all({:conditions => ['cat = ? and sites_id = ? and flags = ?', 'js', session[:accessible_appid], 0]})
    # Get all meta tags informations
    @metas = Metadata.all({:conditions => ['cat = ? and sites_id = ? and flags = ?', 'meta', session[:accessible_appid], 0]})

    @mountedExts = SystemModel.get_list_of_compatible_gems

    # Get all scripts
    @rubies = Metadata.all({:conditions => ['cat = ? and sites_id = ? and flags = ?', 'ruby', session[:accessible_appid], 0]})

    respond_to do |t|
      t.html
    end
  end

  # Show view called "configure" and its view variables
  def configure
    if (@curUserRole == 'developer' ||
        @curUserRole == 'contentadmin' ||
        @curUserRole == 'user' ||
        @curUserRole == 'anonymous' ||
        @curUserRole == 'loggedin')
      raise t(:msg_unauthorized_access)
    end

    cookies[:url] = request.original_url

    @mPage = 'configure'
    @metadatas = Metadata.all({:conditions => ['sites_id = ?', session[:accessible_appid]]})

    @mountedExts = SystemModel.get_list_of_compatible_gems

    respond_to do |t|
      t.html
    end
  end

  # Show view called "file_manager" and its view variables
  def file_manager
    if (@curUserRole == 'contentadmin' ||
        @curUserRole == 'user' ||
        @curUserRole == 'anonymous' ||
        @curUserRole == 'loggedin')
      raise t(:msg_unauthorized_access)
    end

    cookies[:url] = request.original_url

    @mPage = 'file_manager'

    @mountedExts = SystemModel.get_list_of_compatible_gems

    respond_to do |t|
      t.html
    end
  end

  # Show view called "contentlib" and its view variables
  def contentlib
    if (@curUserRole == 'admin' ||
        @curUserRole == 'user' ||
        @curUserRole == 'anonymous'||
        @curUserRole == 'loggedin')
      raise t(:msg_unauthorized_access)
    end

    cookies[:url] = request.original_url

    @mPage = 'contentlib'

    @mountedExts = SystemModel.get_list_of_compatible_gems
  end

  # Show view called "system" and its view variables
  def system
    if (@curUserRole == 'contentadmin' ||
        @curUserRole == 'user' ||
        @curUserRole == 'anonymous' ||
        @curUserRole == 'loggedin')
      raise t(:msg_unauthorized_access)
    end

    cookies[:url] = request.original_url

    constServiceAccountInfoKey = "serviceAccountName"
    @mPage = 'system'

    # List of restricted system items
    @systemRestricteds = %w(serviceAccountName serviceAccountPass currentWorkspace)

    @mountedExts = SystemModel.get_list_of_compatible_gems

    respond_to do |t|
      t.html
    end
  end

  # Show view called "users" and its view variables
  def users
    if (@curUserRole == 'developer' ||
        @curUserRole == 'contentadmin' ||
        @curUserRole == 'user' ||
        @curUserRole == 'anonymous' ||
        @curUserRole == 'loggedin')
      raise t(:msg_unauthorized_access)
    end

    cookies[:url] = request.original_url

    @GROUP_NAME_FILTERABLES = {
        'admin' => 'Administrator',
        'developer' => 'Developer',
        'contentadmin' => 'Content Manager',
        'user' => 'User',
        'anonymous' => 'Anonymous'
    }

    tRoles = Array.new
    allRoles = Role.all
    allRoles.each do |f|
      tRoles << ((@GROUP_NAME_FILTERABLES.map { |k,v| k.strip == f.name.strip ? Hash['id', f.id, 'name', v.strip] : '' }).select { |t| !t.empty? })[0]
    end
    @roles = tRoles.reject { |t| t['name'] == 'User' }

    @mPage = 'users'

    @mountedExts = SystemModel.get_list_of_compatible_gems

    respond_to do |t|
      t.html
    end
  end

  # Show view called "user_profile" and its view variables
  def user_profile
    if (@curUserRole == 'user' ||
        @curUserRole == 'anonymous' ||
        @curUserRole == 'loggedin')
      raise t(:msg_unauthorized_access)
    end

    user = User.find_by_email(session[:accessible_username])
    @username = user.nil? ? '' : user.login.strip
    @email = user.nil? ? '' : user.email.strip

    @mPage = 'user_profile'

    @mountedExts = SystemModel.get_list_of_compatible_gems

    ##
    siteUser = SiteUser.first({ :conditions => ['sites_id = ? and users_id = ?', session[:accessible_appid], user.id ]})
    tRoles = Array.new

    if (siteUser.is_owner)
      tRoles = Role.all.reject { |t| t.name == 'admin' }
    else
      tRoles = Role.all
    end

    @roles = tRoles.reject { |t| t.name == 'user' }
    ##

    respond_to do |t|
      t.html
    end
  end

  # Show view called "extensions" and its view variables
  def extensions

    if (@curUserRole == 'contentadmin' ||
        @curUserRole == 'user' ||
        @curUserRole == 'anonymous' ||
        @curUserRole == 'loggedin')
      raise t(:msg_unauthorized_access)
    end

    cookies[:url] = "#{request.protocol}#{request.host_with_port}#{request.fullpath}"

    extId = params[:id]
    @mPage = 'extensions'
    @mExt = extId

    @mountedExts = SystemModel.get_list_of_compatible_gems

    respond_to do |t|
      t.html
    end

  end

  # Show view called "sites" and its view variables
  def sites

    if (@curUserRole == 'developer' ||
        @curUserRole == 'contentadmin' ||
        @curUserRole == 'user' ||
        @curUserRole == 'anonymous' ||
        @curUserRole == 'loggedin')
      raise t(:msg_unauthorized_access)
    end

    cookies[:url] = "#{request.protocol}#{request.host_with_port}#{request.fullpath}"

    @mPage = 'apps'
    @userSites = get_users_applications
    @userSitesCategories = Metadata.select('distinct key').where({ :cat => 'site'})

    @mountedExts = SystemModel.get_list_of_compatible_gems

    respond_to do |t|
      t.html
    end

  end

  # Show view called "archives" and its view variables
  def archives

    if (@curUserRole == 'anonymous' ||
        @curUserRole == 'loggedin')
      raise t(:msg_unauthorized_access)
    end

    @mPage = "archives"

    @mountedExts = SystemModel.get_list_of_compatible_gems

    respond_to do |t|
      t.html
    end

  end

  private

  def get_current_build
    begin
      file = File.new(Rails.root.join('VERSION'))
      currentVersion = ''
      while (line = file.gets)
        currentVersion << line
      end
      @build_version = currentVersion
      file.close
    rescue => err
      @build_version = '?.?.?'
    end
  end

  def get_current_user_ui_menuitem_visibility
    @ui_menuitem_visibility = YAML.load_file("#{Rails.root}/config/visibility.yml")[@curUserRole]
  end

  def get_site_name(userAppId)
    userAppInfo = Metadata.first({:conditions => ['sites_id = ? and key = ?', userAppId, 'preferences']})
    if (userAppInfo.nil?)
      session[:accessible_appname] = nil
    else
      session[:accessible_appname] = ActiveSupport::JSON.decode(userAppInfo.value)['name'].strip
    end
  end

  def valid_owner?
    @valid_owner = SiteUser.first({ :conditions => ['sites_id = ? and users_id = ? and roles_id = ? and is_owner = ?', session[:accessible_appid], session[:accessible_userid], session[:accessible_roleid], true ]}).nil? ? false : true
  end

  def site_takendown?
    @site_status = Site.find(session[:accessible_appid]).available?
  end

end