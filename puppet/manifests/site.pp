group { 'puppet': ensure => 'present' }

stage { 'databases': before => Stage['rvm'] }
stage { 'rvm': }

class dev-server {
  class { setup_mysql:,      stage => "databases" }
  class { setup_postgresql:, stage => "databases" }
  class { setup_rvm:,        stage => "rvm" }
}

import 'mysql.pp'
import 'postgresql.pp'
import 'rvm.pp'

include dev-server