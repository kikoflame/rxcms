require File.expand_path('../application', __FILE__)
require 'yaml'

# Load Default Configuration
ENV.update YAML.load_file("#{Rails.root}/config/config.yml")[Rails.env]

# Override Configuration with environment specific settings
ENV.update YAML.load_file(ENV['rxcms_config_file'])[Rails.env] if !ENV['rxcms_config_file'].blank? && File.file?(ENV['rxcms_config_file'])

# Initialize the rails application
Rxcms::Application.initialize!