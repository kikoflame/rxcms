# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  # Ubuntu precise 64 VirtualBox (no provisioners)
  config.vm.box = "precise64-nocm"
  config.vm.box_url = "http://puppet-vagrant-boxes.puppetlabs.com/ubuntu-server-12042-x64-vbox4210-nocm.box"

  # Add a tad more memory to the vm.
  config.vm.provider :virtualbox do |vb|
    vb.customize ["modifyvm", :id, "--memory", "1024"]
  end

  # Port mapping for WEBrick
  # @TODO change over to passenger
  config.vm.network :forwarded_port, guest: 3000, host: 3000

  # Give the vm a name
  config.vm.hostname = "dev.rxcms.local"

  # Bootstrap puppet
  # @see https://github.com/hashicorp/puppet-bootstrap for other examples
  config.vm.provision :shell, :path => "shell/bootstrap-ubuntu.sh"
 
  # Provisioning with Puppet
  config.vm.provision :puppet do |puppet|
    puppet.manifests_path = "puppet/manifests"
    puppet.manifest_file = "site.pp"
    puppet.options = '--hiera_config=/etc/hiera.yaml'
  end
end
