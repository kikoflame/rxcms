# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :site_user do
    factory :site_user_admin do
      users_id 1
      sites_id 1
      roles_id 1
      is_owner false
    end

    factory :site_user_dev do
      users_id 5
      sites_id 9999999
      roles_id 2
      is_owner false
    end
  end
end
