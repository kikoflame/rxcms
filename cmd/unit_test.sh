#!/bin/sh

cd ${PWD%/*}

echo 'preparing database for UNIT testing...'
rake db:setup RAILS_ENV=test

echo 'performing UNIT testing...'
rake spec

