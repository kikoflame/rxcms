class AddAvailableToSite < ActiveRecord::Migration
  def up
    add_column :sites, :available, :boolean, :default => true
  end

  def down
    remove_column :sites, :available
  end
end
