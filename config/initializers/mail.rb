ActionMailer::Base.delivery_method = :smtp
ActionMailer::Base.smtp_settings = {
    :address               => ENV['mail_host'], # e.g.: smtp.gmail.com
    :port                  => ENV['mail_port'], # e.g.: 25, 587, etc
    :authentication        => "plain",
    :user_name             => ENV['mail_username'],
    :password              => ENV['mail_password'],
    :enable_starttls_auto  => true
}