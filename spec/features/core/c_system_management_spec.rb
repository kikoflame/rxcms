require 'spec_helper'

feature 'System Management' do
  before do
    sign_in build(:validUser)
  end

  scenario 'User can manage system configuration settings', :integration => true, :js => true do
    visit '/manager/cms/system'
    page.should have_content('System Configurations'), 'System configurations tab missing'
    first('.systemCellEditable').first('.input').set 'vi_VN'
    find('body').click
    page.should have_content('updated'), 'System configurations not changed'
  end

  scenario 'User can filter system configuration settings', :integration => true, :js => true do
    visit '/manager/cms/system'
    fill_in 'txtFilter', :with => 'autoLanguageSwitch'
    page.should_not have_content('currentLocale'), 'Current locale should not appear while user is searching for Language Switch'
  end

  scenario 'User can add new language', :integration => true, :js => true do
    visit '/manager/cms/system'
    page.should have_content('Language Management'), 'Language management tab not found'
    click_link 'Language Management'
    page.should have_content('Supported Languages'), 'Supported language not found'
    click_link 'french'
    page.should have_content('fr_FR'), 'Locale not found'
    click_link 'fr_FR'
    if page.has_text? 'add'
      click_button 'add'
      page.should have_content('locale has been successfully created'), 'locale not registered properly'
    elsif page.has_text? 'delete'
      click_button 'delete'
      page.should have_content('locale has been successfully deleted'), 'locale not deleted properly'
    end
  end

  scenario 'User can manage archived items', :integration => true, :js => true do
    visit '/manager/cms/archives'
    page.should have_content('Archives'), 'archives not found'
  end

  scenario 'User can configure items', :integration => true, :js => true do
    visit '/manager/cms/configure'
    page.should have_content('Debugging Console'), 'configuration page not found'
  end

  scenario 'User can manage extensions', :integration => true, :js => true do
    visit '/manager/cms/extensions'
  end
end