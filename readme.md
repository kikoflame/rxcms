# Introduction

RXCMS is an open-source universal content management system written in Ruby and is based on Rails 3. It allows users to:

* Design and develop web sites in a flash
* Consume and transform data from various sources and serve that data from their web site.

# Setup

## Get the Source

```git clone https://kikoflame@bitbucket.org/kikoflame/rxcms.git```

or

```git clone ssh://git@bitbucket.org:kikoflame/rxcms.git```

## Configuration Settings

Most configuration settings can be changed within the application interface. However, there are some that must be managed via configuration files or environment variables.

### config.yml

The config.yml file is used to load system settings and needs to be configured specifically for your environment before running the application. It is important that you review this file.

You can also set the ```rxcms_config_file``` variable to point to a protected yaml file, which is the preferred method for the production environment.

Note: the yaml file is expected to follow the same format as config.yml

Use Capistrano or a provisioning agent to prepare the environment specific file for your staging or production environment.

Alternatively, you can set environment variables directly in your environment. If you are deploying to Heroku then just set the environment variables using the Heroku toolkit.

### symmetric-encryption.yml

```config/symmetric-encryption.yml```

The file contains example keys and will help you get your development environment running quickly but you must generate the required keys for production yourself.

## Setup Using Vagrant

We have provided a Vagrant file (along with several simple Puppet manifests) to fast-track the setup process.
 
I am assuming you have already Installed and configure [Vagrant](http://docs.vagrantup.com/v2/getting-started/index.html) (v1.3.5 or greater).

1. Navigate to the root project source code directory 
2. Execute command ```vagrant up```
3. Once the booting process is done, execute command ```vagrant ssh``` to enter VM OS bash shell
4. On the guest vm execute the following commands:

    Execute ```rvm use <version>``` to specify your ruby version (1.9.3 or 2.0.0 are supported).

    Execute ```cd /vagrant``` to navigate to project directory (shared from the host system).

    Execute ```bundle install``` to package the gems.

    Execute ```bundle exec rake db:migrate``` to prepare database structure and seed default data.

    Execute ```rails server``` to test the application in development mode
    
## Manual Setup

1. The application requires ruby 1.9.3 or above and rails 3.2.14. I suggest using RVM or similar to configure your ruby environment.
2. From the project root directory:

    Execute ```bundle install``` to package the gems.

    Execute ```bundle exec rake db:migrate``` to prepare database structure and seed default data.

    Execute ```rails server``` to test the application in development mode

# Guide

After installing, you must create an account so that you can manage the system.The first account to be signed up becomes the administrator account.

## Management UI

The Management UI allows you:

* to manage your site's pages, elements, and content including labels and articles.
* upload files that you want to share on your site.
* upload web frameworks that your site uses, such as JQuery plugins.
* manage users and roles
* manage RXCMS plugins

The Management UI leverages [wysiwyghtml5](https://github.com/xing/wysihtml5) and [CodeMirror](http://codemirror.net) for managing and presenting the content and templates.

For more information and a quick tutorial, please visit the [wiki](https://bitbucket.org/kikoflame/rxcms/wiki/Home)

## Elements

The structure of elements are broken down into the following categories:

* Page (containing HTML, JS and other elements)
* Template (containing HTML, JS and other elements)
* Element (containing HTML, JS and other elements) => Tag: `[[(name)? &attr1="value1" &attr2="value2"...]]`
* Script (allowing users/developers to execute raw ruby scripts) => Tag: `[[~(name)? &attr1="value1" &attr2="value2"...]]`
* Placeholder (placed in element) => Tag: `[[$(name)]]`
* Labels and articles (language-sensitive) => Tag: `[[@(name)? &category="(label|article)"]]`

You can use them with HTML tags in your template and it also helps you boost your productivity by providing an easy-to-use HTML/JS editor

## Roles

There are four roles:

* administrator (managing everything),
* developer (managing codes, contents and files),
* content manager (managing contents) and
* user (your end users)

## Extensions

The following extensions have been provided by the RXCMS development team.

### Podio Integration Plugin (rxcms-podio_plugin)

```https://bitbucket.org/kikoflame/rxcms-podio-plugin```

This extension allows users to securely retrieve data from [Podio](http://podio.com) (Podio OAuth Authentication is required) through either web services `{host}/ext/podio/...` or tags defined by the extension developers.

### Database Integration Plugin (rxcms-dbms_plugin)

```https://bitbucket.org/kikoflame/rxcms-dbms-plugin```

Allows users to pull data from existing datasources. The data is securely retrieved (Internal Authentication is required) through either web services `{host}/ext/dbms/...` or tags defined by extension developers.

### Versioning and Peer review Plugin (rxcms-compliance_plugin)

```https://bitbucket.org/kikoflame/rxcms-compliance-plugin```

Allows all content to be versioned and changes can be reviewed and reverted to previous versions. It also includes "peer review" cycle, which, if enabled, will prevent the modifier from being able to publish their own work. Ideal for team leaders who would like to review work before it published.

Anyone can develop extensions. Please check the `Help` section for more information.

### Sharable Templates and Content Plugin (rxcms-sharing_plugin)

```https://bitbucket.org/kikoflame/rxcms-sharing-plugin```

This extension allows users to share templates they create for other users.

## Help

Raise issues or feature requests at [issue tracking](https://bitbucket.org/kikoflame/rxcms/issues?status=new&status=open)

Discuss at this [google group](https://groups.google.com/forum/#!forum/rxcms)

# Roadmap

High level road map of planned features. The order may change based on priority.

M01 - Core CMS (complete)

M02 - Integration with Podio (complete)

M03 - RDMS plugin (complete)

M04 - Version Control, Peer Reviewing (complete)

M05 - Sharable Templates and Elements (complete)

M06 - initial release (complete)

M07 - Content Plugin with Mongo DB

M08 - Domain Hostname Zone Configuration

M09 - Store

M10 - Analytics

# License

See license file

The application uses the following frameworks:

* Bootstrap Framework
* JQuery
* AngularJS

The application uses the following gems:

* Authlogic
* symmetric-encryption
* shoulda-matchers
* rspec
* factory_girl
* simplecov

The application uses the following components:

* CodeMirror
* WysiwygHtml5
