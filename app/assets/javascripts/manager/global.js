function site_operations(appid, action)
{
    $("#siteTakedownCancelBtn").attr("disabled", "disabled");
    $("#siteTakedownBtn").attr("disabled", "disabled");
    console.log(action);
    $.post("/manager/site/services/takedown/manage", {
        "appid" : appid,
        "app_action" : action
    }, function(response){
        if (response.status == "success") {
            if (action == "takedown")
                Messenger().post(window.I18n['msg_site_taken_down']);
            else if (action == "revive")
                Messenger().post(window.I18n['msg_site_revived'])
            location.reload();
        } else
        {
            $("#siteTakedownCancelBtn").removeAttr("disabled");
            $("#siteTakedownBtn").removeAttr("disabled");
            Messenger().post(response.message);
        }

    }).error(function(response){
            $("#siteTakedownCancelBtn").removeAttr("disabled");
            $("#siteTakedownBtn").removeAttr("disabled");
        })
}

$(function () {

    Messenger.options = {
        extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',
        theme: 'air'
    };

    // Site Removal

    $("#removeSiteModal").on("hidden", function () {
        $("#siteRemoveCancelBtn").removeAttr("disabled");
        $("#siteRemoveBtn").removeAttr("disabled");
    });

    $("#siteRemoveBtn").on("click", function (e) {
        e.stopPropagation();

        var id = $(this).attr("data-id");

        if ($("#permaRemoveSite").val().trim() == "remove this site")

            $.ajax({
                url: "/manager/site/services/delete/" + id,
                cache: false,
                type: "DELETE",
                beforeSend: function () {
                    $("#siteRemoveCancelBtn").attr("disabled", "disabled");
                    $("#siteRemoveBtn").attr("disabled", "disabled");
                },
                error: function () {
                    $("#siteRemoveCancelBtn").removeAttr("disabled");
                    $("#siteRemoveBtn").removeAttr("disabled");
                },
                success: function (response) {
                    if (response.status == "success") {
                        $("#removeSiteModal").modal("hide");

                        Messenger().post(window.I18n['msg_site_deleted']);
                    }
                    else if (response.status == "redirect") {
                        // Messenger().post("redirecting...");
                        window.location = response.url;
                    }
                    else if (response.status == "failure") {
                        $("#siteRemoveCancelBtn").removeAttr("disabled");
                        $("#siteRemoveBtn").removeAttr("disabled");
                        Messenger().post(response.message);
                    }
                }
            });

        else
            Messenger().post(window.I18n['msg_no_remove_this_site'])
    });

    // Site Takedown

    $("#takedownSiteModal").on("hidden", function () {
        $("#siteTakedownCancelBtn").removeAttr("disabled");
        $("#siteTakedownBtn").removeAttr("disabled");
    });

    $("#siteTakedownBtn").on("click", function (e) {
        var appid = $(this).attr("data-id"),
            action = $(this).attr("data-action");

        if (action == "takedown")
            if ($("#tempTakedownSite").val().trim() == "take it down")
            {
                site_operations(appid, action);
            } else
                Messenger().post(window.I18n['msg_no_remove_this_site'])
        else if (action == "revive")
            if ($("#tempReviveSite").val().trim() == "revive it")
            {
                site_operations(appid, action)
            } else
                Messenger().post(window.I18n['msg_no_remove_this_site'])
    });

});