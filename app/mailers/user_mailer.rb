class UserMailer < ActionMailer::Base
  default from: ENV['mail_username']

  def reset_passwords_email(user,url,token)
    @user = user
    @url = url
    @token = token
    mail(:to => @user.email, :subject => '[Rx-CMS] Password Reset')
  end

  def activate_account_email(user, url)
    @user = user
    @url = url
    mail(:to => @user.email, :subject => '[Rx-CMS] Account Activation')
  end

  def invite_email(user, email, workspace, url)
    @user = user
    @workspace = workspace
    @url = url
    mail(:to => email, :subect => '[Rx-CMS] Invitation')
  end
end
