function get_random_color(char)
{
    var data = {
        "a" : "#83010b", "b" : "#019a01", "c" : "#2506a2", "d" : "#c5e9df", "e" : "#404040",
        "f" : "#96b2d7", "g" : "#d4096e", "h" : "#cb6502", "i" : "#010101", "j" : "#007ba7",
        "k" : "#336602", "l" : "#e15a01", "m" : "#7e7e7e", "n" : "#ffcb65", "o" : "#d18625",
        "p" : "#c1d41a", "q" : "#c6bc98", "r" : "#db9796", "s" : "#401e03", "t" : "#365e8f",
        "u" : "#9c014f", "v" : "#fd7093", "w" : "#56e224", "x" : "#0f0703", "y" : "#8d4585",
        "z" : "#f8913d"
    };

    return data[char.toLowerCase()];
}

function observeEditState(object){
    var dataEditState = $(object).attr("data-edit-state");

    if (dataEditState == "true")
    {
        $($(object).find("i").first()).removeClass();
        $($(object).find("i").first()).addClass("icon-pencil");
        $(object).attr("data-edit-state","false");
        $($($(object).siblings("span:first")).find("input[type=text]:first")).hide(1, function(){
            $($($(object).siblings("span:first")).find("a:first")).show();
            $($($(object).siblings("span:first")).find("a:first")).focus();
        });

        return false;
    }else
    {
        $($(object).find("i").first()).removeClass();
        $($(object).find("i").first()).addClass("icon-ok");
        $(object).attr("data-edit-state","true");
        $($($(object).siblings("span:first")).find("a:first")).hide(1, function(){
            $($($(object).siblings("span:first")).find("input[type=text]:first")).width($(this).width()+32);
            $($($(object).siblings("span:first")).find("input[type=text]:first")).show();
            $($($(object).siblings("span:first")).find("input[type=text]:first")).val($(this).text());
            $($($(object).siblings("span:first")).find("input[type=text]:first")).focus();
        });

        return true;
    }
}

function getUserAssignedRoles(elem)
{
    $(elem).text(window.I18n['loading']);
    $.getJSON("/manager/r/services/list_profile", function(response){
        if (response.status == "success")
        {
            var html = response.data.join(', ');
            $(elem).text(html);
        }
        else
        {
            Messenger().post(window.I18n['msg_failed_to_load_data']);
        }
    }).error(function(){
        Messenger().post(window.I18n['msg_unable_to_contact_server']);
    });
}

$(function(){

    Messenger.options = {
        extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',
        theme: 'air'
    };

    $("#avatar").css("background", get_random_color($("#aUsername").text().charAt(0)));
    $("#avatar div:first").text($("#aUsername").text().charAt(0).toUpperCase());

    $("#adminRoleAssign").on("click",function(){
        if ($("#selectAssignedRoles").is(":visible"))
        {
            $($(this).find("i:first")).removeClass("icon-ok");
            $($(this).find("i:first")).addClass("icon-pencil");
            $("#selectAssignedRoles").hide();
            $("#assignedRoles").show();
        } else
        {
            $($(this).find("i:first")).removeClass("icon-pencil");
            $($(this).find("i:first")).addClass("icon-ok");
            $("#selectAssignedRoles").show();
            $("#assignedRoles").hide();
        }
    });

    // Load a list of user assigned roles
    getUserAssignedRoles($("#assignedRoles"));

    $("#emailEdit").on("click", function(e){
        e.stopPropagation();
        var $this = $(this);
        var tEmail = $("#emailTxt").val().trim();

        var state = observeEditState($(this));
        if (state == false)
        {
            var updateObject = {
                'email' : $("#emailTxt").val().trim()
            }

            var patt = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/g
            if (patt.test(updateObject.email))
            {
                $.ajax({
                    url: "/manager/users/self/info",
                    cache: false,
                    type: "PUT",
                    data: {
                        'data' : updateObject
                    },
                    beforeSend: function(){
                        $("#emailTxt").attr("disabled","disabled");
                        $("#emailTxt").val(window.I18n['updating']);
                    },
                    error: function(){
                        $("#emailTxt").val($("#aEmail").text());
                        $("#emailTxt").removeAttr("disabled");
                        $("#emailTxt").hide(1, function(){
                            $("#aEmail").show();
                        });
                        Messenger().post(window.I18n['msg_unable_to_contact_server']);
                    },
                    success: function(response){
                        if (response.status == "success")
                        {
                            $("#aEmail").text(tEmail);
                            $("#emailTxt").removeAttr("disabled");
                            $("#emailTxt").hide(1, function(){
                                $("#aEmail").show();
                            });
                            Messenger().post(window.I18n['msg_user_info_updated']);
                        }
                        else
                        {
                            $("#emailTxt").val($("#aEmail").text());
                            $("#emailTxt").removeAttr("disabled");
                            $("#emailTxt").hide(1, function(){
                                $("#aEmail").show();
                            });
                            Messenger().post(window.I18n['msg_user_info_update_failure']);
                        }
                    }
                });
            } else
                Messenger().post(window.I18n['msg_user_invalid_email']);

        }
    });

    $("#delete_account").on("click", function(e){
        e.stopPropagation();

        var $this = $(this);

        if ($("#deleteAccountConfirmation").val().trim() == "remove my account")
            $.ajax({
                url: "/manager/users/" + $this.attr("data-id"),
                cache: false,
                type: "DELETE",
                beforeSend: function(){
                    $("#overlay").fadeIn("fast");
                    $this.attr("disabled","disabled");
                },
                failure: function(){
                    $("#overlay").fadeOut("fast");
                    $this.removeAttr("disabled");
                },
                success: function(response){
                    $("#deleteAccountModal").modal("hide");
                    if (response.status == "success")
                    {
                        window.location = "/manager/cms"
                    }
                    else
                    {
                        $("#overlay").fadeOut("fast");
                        $this.removeAttr("disabled");
                    }
                }
            });
        else
            Messenger().post(window.I18n['msg_no_remove_this_site']);
    });

});
