$(function () {

    Messenger.options = {
        extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',
        theme: 'air'
    };

    $("#settingsList li").on("click", function (e) {

        var $this = $(this);
        var settings = $(".panel");

        $("#settingsList li").removeClass("active");
        $("#panelContainer div").hide();

        if ($(this).hasClass("active"))
            $(this).removeClass("active");
        else
            $(this).addClass("active");

        if ($(this).attr("data-key") != "deletion")
        // Load site data from server
            $.getJSON("/manager/site/services/info/" + $(this).attr("data-id") + "/" + $(this).attr("data-key"),function (response) {
                if (response.status == "success") {
                    data = JSON.parse(response.data.value);

                    $("#sitename").val(data.name);
                    $("#sitedesc").val(data.description);
                    $("#sitecreationdate").text((new Date(response.data.created_at)).toUTCString());
                    $("#prefsSaveBtn").attr("data-id", response.data.id);

                    $($("#panelContainer").find("#" + $this.attr("data-key"))).show();
                    $($($("#panelContainer").find("#" + $this.attr("data-key"))).find("*")).show();
                }
                else {
                    Messenger().post(window.I18n['msg_failed_to_load_data']);
                }
            }).error(function () {
                    Messenger().post(window.I18n['msg_unable_to_contact_server']);
                });
        else
            alert("deletion is pending");
    });

    $("#prefsSaveBtn").on("click", function (e) {
        var siteid = $(this).attr("data-id");
        var selectedid = $("#sites").val();
        var updatedData = {
            'name': $("#sitename").val().trim(),
            'description': $("#sitedesc").val().trim()
        }
        var dataObj = {
            'value': JSON.stringify(updatedData)
        }

        if (updatedData.name.trim().length > 6) {
            $.ajax({
                url: "/manager/configurations/" + siteid,
                cache: false,
                type: "PUT",
                data: {
                    'data': dataObj,
                    'flag': 'site'
                },
                error: function () {
                    $("#prefsSaveBtn").removeAttr("disabled");
                    $("#prefsSaveBtn").text("save changes");
                    Messenger().post(window.I18n['msg_unable_to_contact_server']);
                },
                beforeSend: function () {
                    $("#prefsSaveBtn").attr("disabled", "disabled");
                    $("#prefsSaveBtn").text(window.I18n['saving']);
                },
                success: function (response) {
                    if (response.status == 'success') {
                        $("#prefsSaveBtn").removeAttr("disabled");
                        $("#prefsSaveBtn").text(window.I18n['save_changes']);

                        // Update sitename in top bar
                        $("#brandSitename").text(response.extra.new_sitename.length > 16 ? response.extra.new_sitename.slice(0,17) + "..." : response.extra.new_sitename);
                        Messenger().post(window.I18n['msg_site_updated']);
                    }
                    else {
                        $("#prefsSaveBtn").removeAttr("disabled");
                        $("#prefsSaveBtn").text("save changes");
                        Messenger().post(window.I18n['msg_site_update_failure']);
                    }
                }
            });
        } else if (updatedData.name.trim().length < 6 && updatedData.name.trim().length >= 0) {
            Messenger().post(window.I18n['msg_site_name_error']);
        }
        /* else
         {
         Messenger().post("name cannot be empty");
         } */

    });

    $($("#settingsList").find("li[data-key=preferences]")).click();

});
