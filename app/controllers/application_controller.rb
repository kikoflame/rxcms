class ApplicationController < ActionController::Base
  #protect_from_forgery

  @@abbrErr = "APP"

  # if config.consider_all_requests_local is false, will capture all errors. Some error templates are in public folder

  unless Rails.application.config.consider_all_requests_local
    rescue_from Exception, :with => :show_error_500
  end

  before_filter :manage_browser, :called_controller, :current_translations

  protected

  # Internal protected methods
  def manage_browser
    if (params[:controller] == 'manager/manager' and detect_browser == 'mobile')
      raise t(:msg_access_denied_mobile_browser_not_compatible)
    end
  end

  def called_controller
    @called = request.referrer
  end
  # END

  private

  def current_translations
    I18n.backend.send(:init_translations) unless I18n.backend.initialized?

    hLocale = request.headers['Accept-Language'].nil? ? nil : request.headers['Accept-Language'].scan(/^[a-z]{2}/).first

    if (!cookies[:adminLocale].nil? && !cookies[:adminLocale].empty?)
      I18n.locale = cookies[:adminLocale].to_s.strip.scan(/^[a-z]{2}/).first
    elsif (!hLocale.nil? && !hLocale.empty?)
      I18n.locale = hLocale
    else
      I18n.locale = 'en'
    end

    translations = I18n.backend.send(:translations)
    if (translations[I18n.locale].nil?)
      I18n.locale = 'en'
    end
    @oTranslations = translations[I18n.locale].with_indifferent_access
  end

  # Should be tested to see if all raised exceptions are caught and shown to developers/users

  def show_error_500(exception)
    render :template => '/errors/500.html.erb', :layout => 'error', :status => 500, :locals => {
        :msg => !exception.nil? ? "#{exception.message}" : "", :status => 500
    }
  end

  # Detect and redirect to mobile feature

  MOBILE_BROWSERS = ["playbook", "windows phone", "android", "ipod", "iphone", "opera mini", "blackberry", "palm","hiptop","avantgo","plucker", "xiino","blazer","elaine", "windows ce; ppc;", "windows ce; smartphone;","windows ce; iemobile", "up.browser","up.link","mmp","symbian","smartphone", "midp","wap","vodafone","o2","pocket","kindle", "mobile","pda","psp","treo"]

  def detect_browser
    agent = request.headers["HTTP_USER_AGENT"].nil? ? 'unknown' : request.headers["HTTP_USER_AGENT"].downcase

    MOBILE_BROWSERS.each do |m|
      return "mobile" if agent.match(m)
    end
    return "desktop"
  end

end
