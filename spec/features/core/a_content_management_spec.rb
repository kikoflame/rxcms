require 'spec_helper'

feature 'Content Management' do
  before :all do

  end

  after :all do

  end

  scenario "User can see front end", :integration => true, :core => true, :js => true do
    sign_in build(:validDevUser)
    page.should have_content('Getting Started'), "Getting Started portal missing"
    find("#brandSitename").trigger('click')
    current_url.should include("/manager/cms/home")
    # page.should have_content('blank page'), "Front end not loaded properly"
  end

  scenario "User can upload and download file", :integration => true, :js => true do
    FileUtils.mkdir(Rails.root.join("public","resources",'AIS33'))
    FileUtils.mkdir(Rails.root.join("public","resources",'AIS33',"downloads"))
    FileUtils.copy_file("#{Rails.root.join("spec","fixtures","files")}/rspec.png","#{Rails.root.join("public","resources",'AIS33')}/downloads/rspec.png")
    visit("/AIS33/client/dl/rspec.png")
    FileUtils.rm_rf(Dir.glob("#{Rails.root.join("public","resources",'AIS33')}/*"))
    FileUtils.rm_rf(Rails.root.join("public","resources",'AIS33'))
  end

  scenario 'Developer can create page', :integration => true, :js => true do
    sign_in build(:validDevUser)
    visit('/manager/cms/pages')
    # click_link '+'
    # page.should have_content('Create Page'), 'Create page modal does not show'
    # first("#createModalName").set 'index'
    # click_button 'close'
    # click_button 'save'
    # page.should have_content('created'), 'Page created failed'
  end

  scenario 'Developer can manage elements', :integration => true, :js => true do
    sign_in build(:validDevUser)
    visit('/manager/cms/elements')
  end

  scenario 'Developer can create, modify and delete metatag', :integration => true, :js => true do
    # create
    sign_in build(:validDevUser)
    visit('/manager/cms/elements')
    find(".metatagCreate").click
    page.should have_selector('#metatagCreate', :visible => true)
    page.should have_content("Create Metatag"), "Create Metatag dialog not shown"
    find("#metatagCreate_name").set 'metatag'
    click_on 'create'
    # page.should have_content("metatag"), "element metatag not created"

    # modify
    click_link "metatag"
    click_on "save"

    # delete
    click_link "metatag"
    find(".metatagDelete").click
    page.should have_selector("#metatagDelete", :visible => true)
    page.should have_content("Delete Metatag"), "Delete Metatag dialog not found"
    click_on 'delete'
    # page.should have_content("deleted"), "element metatag not deleted"
  end

  scenario 'Developer can create placeholder', :integration => true, :js => true do
    sign_in build(:validDevUser)
    visit('/manager/cms/elements')
    find(".placeholderCreate").click
    page.should have_selector('#placeholderCreate', :visible => true)
    page.should have_content("Create Placeholder"), "Create Placeholder dialog not shown"
    find("#placeholderCreate_name").set 'placeholder'
    click_on 'create'
    # page.should have_content("placeholder"), "element placeholder not created"
  end

  scenario 'Developer can create, modify and delete script', :integration => true, :js => true do
    sign_in build(:validDevUser)
    visit('/manager/cms/elements')
    find(".scriptCreate").click
    page.should have_selector('#scriptCreate', :visible => true)
    page.should have_content("Create Script"), "Create Script dialog not shown"
    find("#scriptCreate_name").set 'script'
    click_on 'create'
    # page.should have_content("script"), "element script not created"

    # modify
    click_link "script"
    click_on "save"

    # delete
    click_link "script"
    find(".scriptDelete").click
    page.should have_selector("#scriptDelete", :visible => true)
    page.should have_content("Delete Script"), "Delete Script dialog not shown"
    click_on 'delete'
    # page.should have_content("deleted"), "element script not deleted"
  end

  scenario 'Developer can create, modify and delete template', :integration => true, :js => true do
    sign_in build(:validDevUser)
    visit('/manager/cms/elements')
    find(".templateCreate").click
    page.should have_selector('#templateCreate', :visible => true)
    page.should have_content("Create Template"), "Create Template dialog not shown"
    find("#templateCreate_name").set 'template'
    click_on 'create'
    # page.should have_content("template"), "element template not created"

    # modify
    click_link "template"
    click_on "save"

    # delete
    click_link "template"
    find(".templateDelete").click
    page.should have_selector("#templateDelete", :visible => true)
    click_on 'delete'
    # page.should have_content("deleted"), "element template not deleted"
  end

  scenario 'Developer can activate template elements', :integration => true, :js => true do
    sign_in build(:validDevUser)
    visit('/manager/cms/elements')
    click_link "template"
    page.should have_content("save"), "save button not shown"
    within("#styleLabels") do
      first("span").trigger("click")
    end
  end

  scenario 'Developer can deactivate template elements', :integration => true, :js => true do
    sign_in build(:validDevUser)
    visit('/manager/cms/elements')
    click_link "template"
    page.should have_content("save"), "save button not shown"
    within("#styleLabels") do
      first("span").trigger("click")
    end
  end

  scenario 'Developer can manage contents', :integration => true, :js => true do
    sign_in build(:validDevUser)
    visit('/manager/cms/contentlib')
  end

  scenario 'Developer can create, modify and delete labels or articles', :integration => true, :js => true do
    sign_in build(:validDevUser)

    # create
    visit('/manager/cms/contentlib')
    click_link 'Labels'
    page.should have_selector("#newLabelsNameTxt", :visible => true)
    page.should have_selector("#newLabelsContentTxt", :visible => true)
    page.should have_content("Key"), "key not shown"
    first("#newLabelsNameTxt").set 'test-label'
    first("#newLabelsContentTxt").set 'test content'
    find("#aNewLabels").click
    # page.should have_content('created'), 'label not created properly'

    # modify
    page.should have_selector('input[data-attr=editor]'), 'created item not found'
    first("input[data-attr=editor]").set 'new key'
    find("body").click
    # page.should have_content('updated'), 'label not updated properly'

    # delete
    first(".labelDelete").click
    page.should have_selector("#labelsDelete", :visible => true)
    page.should have_content('Delete Label'), 'label not updated properly'
    click_on 'Delete'
    # page.should have_content('deleted'), 'label not properly deleted'
  end
end