class AddScopeToRelease < ActiveRecord::Migration
  def up
    add_column :releases, :scope, :string, :default => "w"
  end

  def down
    remove_column :releases, :scope
  end
end
