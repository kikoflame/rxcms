class Release < ActiveRecord::Base
  attr_accessible :date_time, :metadata_id, :users_id, :scope

  belongs_to :metadata
end