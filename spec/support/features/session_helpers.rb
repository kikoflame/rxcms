module Features
  module SessionHelpers

    def sign_up(user)
      visit('/manager/cms')
      #page.save_screenshot('screenshot-sign_up1.png')
      within('#loginForm') do
        fill_in('email', :with => user.email)
        fill_in('password', :with => user.password)
        if page.has_field?('password_confirmation', :type => 'password')
          fill_in('login', :with => user.login)
          fill_in('password_confirmation', :with => user.password)
          fill_in('newApp', :with => 'New Application')
        end
        click_button('loginFormSubmit')
        #page.save_screenshot('screenshot-sign_up2.png')
      end
    end

    def sign_in(user)
      visit('/manager/cms')
      #page.save_screenshot('screenshot-sign_in1.png')
      within('#loginForm') do
        fill_in('email', :with => user.email)
        fill_in('password', :with => user.password)
        click_button('loginFormSubmit')
        #page.save_screenshot('screenshot-sign_in2.png')
      end
    end
  end
end