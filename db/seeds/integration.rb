ActiveRecord::Base.establish_connection
case ActiveRecord::Base.connection.adapter_name
  when 'SQLite'
    # SQLite
    ActiveRecord::Base.connection.execute("DELETE FROM roles")
    ActiveRecord::Base.connection.execute("DELETE FROM sqlite_sequence WHERE name = 'roles'")
  when 'MySQL'
    # MySQL
    ActiveRecord::Base.connection.execute("TRUNCATE roles")
  when 'PostgreSQL'
    # PostgreSQL
    ActiveRecord::Base.connection.execute("TRUNCATE roles")
  else
    raise '[Err] unsupported database adapter'
end

roleAdmin = Role.create({
                :name => 'admin'
            })
roleDev = Role.create({
                :name => 'developer'
            })
roleContentadmin = Role.create({
                :name => 'contentadmin'
            })
roleUser = Role.create({
                :name => 'user'
            })
dev = User.create({
                :login => 'dev',
                :password => 'devpass',
                :password_confirmation => 'devpass',
                :email => 'dev@host.com'
            })
admin = User.create({
                :login => 'admin',
                :password => 'password',
                :password_confirmation => 'password',
                :email => 'admin@host.com'
                    })
admin.activate!
dev.activate!
devSite = Site.create({
                :sites_id => 'AIS33',
                :available => true
            })
adminSite = Site.create({
                :sites_id => 'ADD01',
                :available => true
                        })
devSiteuser = SiteUser.create({
                :sites_id => devSite.id,
                :users_id => dev.id,
                :roles_id => roleDev.id,
                :is_owner => false
                })
adminSiteuser = SiteUser.create({
                :sites_id => adminSite.id,
                :users_id => admin.id,
                :roles_id => roleAdmin.id,
                :is_owner => true
                                })
devTest = Metadata.create({
                  :cat => 'placeholder',
                  :key => 'devtestpl',
                  :value => 'test value',
                  :sites_id => devSite.id,
                  :mime => 'text/html'
                          })
templateSample = Metadata.create({
                  :cat => 'template',
                  :key => 'devtemplate',
                  :value => '[[body]]',
                  :sites_id => devSite.id,
                  :mime => 'text/html'
                 })
devTestIndexPage = Metadata.create({
                  :cat => 'page',
                  :key => 'index',
                  :value => '[[devtestpl]][[~devscript]][[@languageItem? &category="label"]]',
                  :sites_id => devSite.id,
                  :mime => 'text/html'
                                   })
devRscript = Metadata.create({
                  :cat => 'ruby',
                  :key => 'devscript',
                  :value => '# some script',
                  :sites_id => devSite.id,
                  :mime => 'text/html'
                             })
devSmeta = Metadata.create({
                  :cat => 'meta',
                  :key => 'meta',
                  :value => '<!-- no content -->',
                  :sites_id => devSite.id,
                  :mime => 'text/html'
                           })
devSjs = Metadata.create({
                  :cat => 'js',
                  :key => 'js',
                  :value => '//no content',
                  :sites_id => devSite.id,
                  :mime => 'text/html'
                         })
devScss = Metadata.create({
                  :cat => 'css',
                  :key => 'css',
                  :value => '/* no content */',
                  :sites_id => devSite.id,
                  :mime => 'text/html'
                          })
devLanguageitem = Metadata.create({
                  :cat => 'label',
                  :key => 'languageItem',
                  :value => 'some language item',
                  :sites_id => devSite.id,
                  :mime => 'text/plain'
                                  })
enUK = Metadata.create({
                  :cat => 'locale',
                  :key => 'English/UK',
                  :value => 'en_UK',
                  :sites_id => devSite.id,
                  :mime => 'text/plain'
                       })
devCurrentlocale = Metadata.create({
                  :cat => 'config',
                  :key => 'currentLocale',
                  :value => 'en_US',
                  :sites_id => devSite.id,
                  :mime => 'text/plain'
                                   })
devSitemeta = Metadata.create({
                  :key => 'preferences',
                  :value => '{"name":"dev site"}',
                  :cat => 'site',
                  :mime => 'text/plain',
                  :sites_id => devSite.id
                              })
adminSitemeta = Metadata.create({
                                    :key => 'preferences',
                                    :value => '{"name":"admin site"}',
                                    :cat => 'site',
                                    :mime => 'text/plain',
                                    :sites_id => adminSite.id
                                })
MetadataAssociation.create({
    :destId => templateSample.id,
    :srcId => devTestIndexPage.id
                           })
MetadataAssociation.create({
    :destId => devSmeta.id,
    :srcId => templateSample.id
                           })
MetadataAssociation.create({
    :destId => devSjs.id,
    :srcId => templateSample.id
                           })
MetadataAssociation.create({
    :destId => devScss.id,
    :srcId => templateSample.id
                           })