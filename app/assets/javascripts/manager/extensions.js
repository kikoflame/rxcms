$(function(){

    var extension_url = $($($("#extensionsList li.active").first()).find("a:first")).attr("data-link");
    var extension_installer = $($($("#extensionsList li.active").first()).find("a:first")).attr("data-installer");

    if (extension_url != undefined && extension_url.length > 0)
    {
        $("#body").addClass("container");
        $("#body").css("text-align","center");
        $("#body").css("padding","45px 0 45px 0");
        $("#body").html("<h4>" + window.I18n['msg_loading_extension'] + "</h4>");

        $.get(extension_url, function(response){
            $("#body").removeClass("container");
            $("#body").css("text-align","");
            $("#body").css("padding","");
            $("#body").html(response);
        }).error(function(){
            $("#body").html("<h4>" + window.I18n['msg_loading_extension_installer'] + "</h4>");

            if (extension_installer != undefined && extension_installer.length > 0)
            {
                $.get(extension_installer, function(response){
                    $("#body").removeClass("container");
                    $("#body").css("text-align","");
                    $("#body").css("padding","");
                    $("#body").html(response);
                }).error(function(){
                    $("#body").html("<h4>" + window.I18n['msg_unable_to_load_extension'] + "</h4>");
                });
            } else
            {
                $("#body").html("<h4>" + window.I18n['msg_extension_has_no_installer'] + "</h4>");
            }
        });
    }
    else
    {
        $("#body").addClass("container");
        $("#body").css("text-align","center");
        $("#body").css("padding","45px 0 45px 0");
        $("#body").html("<h3>" + window.I18n['msg_no_extensions'] + "</h3><h4>" + window.I18n['msg_extension_have_not_chosen'] + "</h4>");
    }

});