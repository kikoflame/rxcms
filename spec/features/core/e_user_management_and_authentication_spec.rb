require 'spec_helper'

feature "User Management and Authentication" do
  before :each do
    sign_in build(:validUser)
  end

  # scenario "User can sign up", :integration => true, :core => true, :js => true do
  #  page.should have_content('Getting Started'), "Getting Started portal missing"
  #  page.should have_content('Support'), "Support portal missing"
  #  end

  scenario "User can sign in with correct credentials and see home page", :integration => true, :core => true, :js => true do
    page.should have_content('Getting Started'), "Getting Started portal missing"
  end

  scenario "User can sign in with previous session", :integration => true, :js => true do
    visit('/manager/cms')
    click_link "admin@host.c..."
    click_link "log out"
    current_path.should include("/manager/cms")
    sign_up build(:validUser)
    current_path.should include("/manager/cms/home")
  end

  scenario "User can reset password", :integration => true, :js => true do
    visit('/manager/cms')
    click_link "admin@host.c..."
    click_link "log out"
    current_path.should include("/manager/cms")
    all("a")[1].click
    page.should have_content("Password Reset"), "Password Reset modal not found"
    find("#resetEmail").set 'admin@host.com'
    click_button 'proceed'
    current_url.should include("/manager/cms")
  end

  scenario "Administrator can sign in with correct credentials and see manage users", :integration => true, :js => true do
    visit("/manager/cms/users")
    page.should have_content('USER GROUPS'), "User Groups label not found"
  end

  scenario "User can manage profile", :integration => true, :js => true do
    visit("/manager/cms/profile")
    page.should have_content("General Information"), "General Information not found"
  end

  scenario "User can log out", :integration => true, :js => true do
    visit("/manager/cms")
    click_link "admin@host.c..."
    click_link "log out"
    current_path.should include("/manager/cms")
  end

  scenario "Administrator can see user management page", :integration => true, :js => true do
    visit("/manager/cms/users")
    page.should have_content("USER OPERATIONS"), "User operation not found"
  end

  scenario "Administrator can see user management page and invite user", :integration => true, :js => true do
    visit("/manager/cms/users")
    first("#userSearchText").set "test@untested.com"
    keypress_script = "var e = $.Event('keyup', { keyCode: 13 }); $('#userSearchText').trigger(e);"
    page.driver.execute_script(keypress_script)
    click_link "invite"
  end

  scenario "Administrator can create user", :integration => true, :js => true do
    visit("/manager/cms/users")
    click_link "create"
    page.should have_content("Create User"), "create user not found"
    page.should have_selector("#userCreation", :visible => true)
    first("#newLogin").set "testerisgood"
    first("#newEmail").set "tester@host.com"
    first("#newPassword").set "123456"
    first("#newPasswordConfirmation").set "123456"
    click_button "save"
  end

  scenario "Administrator can see user", :integration => true, :js => true do
    visit("/manager/cms/users")
    click_link "Administrator"
  end

  scenario "User can delete account", :integration => true, :js => true do
    visit("/manager/cms/profile")
    click_link "Delete Account"
    page.should have_content("Delete User"), "Modal Delete User not found"
    first("#deleteAccountConfirmation").set "remove my account"
    click_button "proceed"
    current_path.should  include("/manager/cms/profile")
  end

end