#!/bin/bash
#
# A simple bootstrap script to install puppet and required modules
# @see https://github.com/hashicorp/puppet-bootstrap for other examples
set -e

install() {
  DISTRIB_CODENAME=$(lsb_release --codename --short)
  DEB="http://apt.puppetlabs.com/puppetlabs-release-${DISTRIB_CODENAME}.deb"
  DEB_PATH=$(mktemp)

  wget --output-document=$DEB_PATH $DEB
  dpkg -i $DEB_PATH

  apt-get update > /dev/null
  apt-get install --yes puppet rubygems > /dev/null
  gem install --no-ri --no-rdoc rubygems-update 
  update_rubygems > /dev/null

  puppet module install puppetlabs-mysql
  puppet module install puppetlabs-postgresql
  puppet module install maestrodev-rvm
}

if which puppet > /dev/null ; then
  echo "Puppet is already installed"
else
  install
fi