require 'spec_helper'

feature 'Site Management' do
  before :each do
    sign_in build(:validUser)
  end

  scenario 'Administrator can manage current site information', :integration => true, :js => true do
    visit('/manager/cms/sites')
    first("#sitename").set 'modified'
    first("#sitedesc").set 'modified'
    click_button 'save changes'
    page.should have_content('successfully'), 'site information not saved successfully'
  end

  scenario 'Administrator can take down and restore site', :integration => true, :js => true do
    visit('/manager/cms/sites')
    click_link 'System'
    click_link 'Take Site Down'
    page.should have_content('Site Takedown'), 'Site takedown modal does not appear'
    first("#tempTakedownSite").set 'take it down'
    click_button 'proceed'
    page.should have_content('taken down'), 'site taken down failed'
    visit('/manager/cms/sites')
    click_link 'System'
    click_link 'Restore Site'
    page.should have_content('Site Restoration'), 'Site restoration modal does not appear'
    first("#tempReviveSite").set 'revive it'
    click_button 'proceed'
    # page.should have_content('operational'), 'Site restored failed'
  end

  # scenario 'User can change sites rapidly', :integration => true, :js => true do
  #  visit('/manager/cms/sites')
  #  page.should have_content("modified"), "Site not created"
  #  first("#menuItemAppSelection").click
  #  current_url.should include('/manager/cms/apps')
  # end

  scenario 'User can create site rapidly', :integration => true, :js => true do
    visit('/manager/cms/apps')
    click_link 'register a site'
    page.should have_selector("#siteRegistration", :visible => true)
    first("#siteModalName").set 'Demo Site'
    first("#siteModalDescription").set 'Demo Site description'
    first("#siteModalCreateBtn").trigger("click")
    current_url.should include('/manager/cms')
  end

  scenario 'If no site were selected, user cannot switch site', :integration => true, :js => true do
    visit('/manager/cms/apps')
    click_button 'proceed'
    current_url.should include('/manager/cms/apps')
    page.should have_content('no site was selected'), 'no site label not found'
  end

  scenario 'Z-Administrator can delete site', :integration => true, :js => true do
    visit('/manager/cms/sites')
    click_link 'System'
    page.should have_content('Delete Site'), 'Delete site not found'
    click_link 'Delete Site'
    page.should have_selector('#removeSiteModal'), 'Site delete modal does not appear'
    first("#permaRemoveSite").set 'remove this site'
    click_button 'proceed'
    current_url.should include('/manager/cms/sites')
  end
end