$(function () {
    Messenger.options = {
        extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',
        theme: 'air'
    };

    if (window.location.hash == '#userRegistration')
    {
        $("#userRegistration").modal("show");
    }

    $("#loginForm").submit(function(){
        $("#loginFormSubmit").attr("disabled","disabled");
    });

    $("#resetPasswordModal").on("shown", function () {
        // don't do anything
    });

    $("#resetPasswordModal").on("hidden", function () {
        $("#resetEmail").val("");
    });

    $("#resetPasswordDetailedModal").on("shown", function () {
        // don't do anything
    });

    $("#resetPasswordDetailedModal").on("hidden", function () {
        $("#requestCode").val("");
        $("#resetPassword").val("");
        $("#resetPasswordConfirmation").val("");
    });

    $("#beginResetPasswordBtn").on("click", function () {
        var $this = $(this)
        if ($("#resetEmail").val().length > 0) {
            $($this.prev("button")).hide();
            $this.text(window.I18n['processing_please_wait']);
            $this.attr("disabled", "disabled");
            $.post("/manager/u/services/request_pass_reset",
                {
                    "email": $("#resetEmail").val().trim()
                },function (response) {
                    if (response.status == "success") {
                        $('#resetPasswordModal').modal('hide');
                        $this.text(window.I18n['proceed']);
                        $this.removeAttr("disabled");
                        $($this.prev("button")).show();
                        $('#resetPasswordDetailedModal').modal({ backdrop : "static", keyboard : false });
                        $('#resetPasswordDetailedModal').modal('show');
                        Messenger().post(window.I18n['msg_password_reset_step2']);
                    }
                    else {
                        $this.text(window.I18n['proceed']);
                        $this.removeAttr("disabled");
                        $($this.prev("button")).show();
                        Messenger().post(response.message);
                    }

                }).error(function () {
                    $this.text(window.I18n['proceed']);
                    $this.removeAttr("disabled");
                    $($this.prev("button")).show();
                    Messenger().post(window.I18n['msg_unable_to_contact_server'])
                });
        } else
            Messenger().post(window.I18n['msg_password_reset_email_not_empty']);
    });

    $("#doResetPasswordBtn").on("click", function () {
        var $this = $(this);
        var requestCode = $("#requestCode").val(),
            newPassword = $("#resetPassword").val(),
            newPasswordConfirmation = $("#resetPasswordConfirmation").val();

        $("#doResetPasswordBtn").text(window.I18n['processing_please_wait']);
        $("#doResetPasswordBtn").attr("disabled", "disabled");
        $($this.prev("button")).hide();
        if (requestCode.length > 0) {
            if (newPassword.length > 0 && newPasswordConfirmation.length > 0) {
                $.post("/manager/u/services/do_pass_reset", {
                    "requestCode": requestCode,
                    "newPassword": newPassword,
                    "newPasswordConfirmation": newPasswordConfirmation
                }, function (response) {
                    if (response.status == "success") {
                        $("#doResetPasswordBtn").text(window.I18n['proceed']);
                        $("#doResetPasswordBtn").removeAttr("disabled");
                        $($this.prev("button")).show();
                        $("#resetPasswordDetailedModal").modal('hide');
                        Messenger().post(window.I18n['msg_password_reset_success']);
                    }
                    else {
                        $("#doResetPasswordBtn").text(window.I18n['proceed']);
                        $("#doResetPasswordBtn").removeAttr("disabled");
                        $($this.prev("button")).show();
                        Messenger().post(response.message);
                    }
                })
            }
            else {
                $("#doResetPasswordBtn").text(window.I18n['proceed']);
                $("#doResetPasswordBtn").removeAttr("disabled");
                $($this.prev("button")).show();
                Messenger().post(window.I18n['msg_password_reset_missing'])
            }
        }
        else {
            $("#doResetPasswordBtn").text(window.I18n['proceed']);
            $("#doResetPasswordBtn").removeAttr("disabled");
            $($this.prev("button")).show();
            Messenger().post(window.I18n['msg_password_request_missing']);
        }
    });

    $("#userRegistration").on("hidden", function(){
        $("#adminModalAccountName").val("");
        $("#adminModalAccountPass").val("");
        $("#adminModalAccountPassConfirm").val("");
        $("#btnUserRegCancelBtn").removeAttr("disabled");
        $("#btnUserRegCreateBtn").removeAttr("disabled");
        $("#btnUserRegCreateBtn").text(window.I18n['create']);
        // $("#captchaText").text("...");
    });

    $("#userRegistration").on("shown", function(){
        $($(this).find("#btnUserRegCreateBtn")).removeAttr("disabled");
        $($(this).find("#btnUserRegCancelBtn")).removeAttr("disabled");
    });

    $("#btnUserRegCreateBtn").on("click", function(e){
        e.stopPropagation();

        var errors = "",
            $this = $(this);
        /*if ($("#adminModalAccountName").val().length < 5)
            errors += "login name is too short or empty<br />";
        if ($("#adminModalAccountEmail").val().length > 0) {
            var emailPattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/

            if (!emailPattern.test($("#adminModalAccountEmail").val()))
                errors += "login email is not valid<br />";
        } else
            errors += "login email is empty<br />";
        if ($("#adminModalAccountCaptcha").val().length == 0)
            errors += "captcha is not filled out<br />";
        if ($("#adminModalAccountPass").val().length < 4)
            errors += "login password is too short (4 chars) or empty<br />";
        if ($("#adminModalAccountPass").val().indexOf(' ') >= 0)
            errors += "login password contains spaces<br />";
        if ($("#adminModalAccountPassConfirm").val().length == 0)
            errors += "login password (confirmed) is empty<br />";
        if ($("#adminModalAccountPassConfirm").val() != $("#adminModalAccountPass").val())
            errors += "login passwords don't match<br />"*/

        if (errors.length > 0)
            Messenger().post(errors);
        else
        {
            var newUser = {
                "login" : $("#adminModalAccountName").val().trim(),
                "email" : $("#adminModalAccountEmail").val().trim(),
                "password" : $("#adminModalAccountPassConfirm").val(),
                "password_confirmation": $("#adminModalAccountPassConfirm").val()
            };

            $this.attr("disabled","disabled");
            $("#btnUserRegCancelBtn").attr("disabled","disabled");

            $.post("/manager/users/only", {
                "newUser" : newUser
            }, function(response){
                if (response.status == 'success')
                {
                    $("#userRegistration").modal("hide");
                    Messenger().post({ message : window.I18n['msg_user_has_been_created'],
                        hideAfter : 3600,
                        showCloseButton: true });
                }
                else
                {
                    Messenger().post({ message : window.I18n['msg_user_unable_to_process_request'],
                        type : "error" });
                    $this.removeAttr("disabled");
                    $("#btnUserRegCancelBtn").removeAttr("disabled");
                }
            }).error(function(){
                    Messenger().post({ message : window.I18n['msg_unable_to_contact_server'],
                        type : "error" });
                    $this.removeAttr("disabled");
                    $("#btnUserRegCancelBtn").removeAttr("disabled");
                });

        }
    });
});